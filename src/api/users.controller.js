const debug = require('debug')('geoportal-server:users.controller');
const Joi = require('@ahapi/joi');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const UsersDAO = require('../dao/usersDAO');
const Email = require('email-templates');
const Roles = require('../_helpers/roles');
const useragent = require('useragent');
const cache = require('../../redis');
var geoip = require('geoip-lite');
const multer = require('multer');
const profileImages = require('../../storage/profile-images');
const getListOfObjectsWithErrorMessages = require('../_helpers/errors');
const hashPassword = async password => bcrypt.hash(password, 10);

class User {
  constructor({
    id,
    birth_date,
    email,
    first_name,
    last_name,
    gender,
    locale = 'ru',
    region,
    role = Roles.User,
    password,
    image,
    image_url,
    created_at,
    updated_at,
    is_verified,
  } = {}) {
    this.username = email;
    this.birth_date = birth_date;
    this.email = email;
    this.first_name = first_name;
    this.last_name = last_name;
    this.gender = gender;
    this.locale = locale;
    this.region = region;
    this.role = role;
    this.image = image;
    this.image_url = image_url;
    this.created_at = created_at;
    this.updated_at = updated_at;
    this.password = password;
    this.id = id;
    this.is_verified = is_verified;
  }

  toJson() {
    return {
      id: this.id,
      first_name: this.first_name,
      last_name: this.last_name,
      username: this.username,
      birth_date: this.birth_date,
      email: this.email,
      gender: this.gender,
      locale: this.locale,
      role: this.role,
      image: this.image,
      image_url: this.image_url,
      created_at: this.created_at,
      updated_at: this.updated_at,
    };
  }

  async comparePassword(plainText) {
    return await bcrypt.compare(plainText, this.password);
  }

  encoded() {
    const tokenTTL = process.env.TOKEN_TTL;

    if (!tokenTTL) {
      throw new Error('Specify TTL for user access token');
    }

    return jwt.sign(
      {

        //FIXME: uncomment this line on production
        exp: Math.floor(Date.now() / 1000) + parseInt(tokenTTL),
        ...this.toJson(),
      },
      process.env.SECRET_KEY,
    );
  }

  async sendVerificationMail() {
    const token = jwt.sign(
      {
        exp: Math.floor(Date.now() / 1000) + 60 * 60 * 12,
        ...this.toJson(),
      },
      process.env.SECRET_KEY,
    );

    //FIXME: uncomment this two lines on production
    // const baseUrl = process.env.BASE_URL || `http://${process.env.IP}:${process.env.PORT}`;
    // const url = `${baseUrl}/api/v2/users/wf?upn=${token}`;
    
    //FIXME: commment this line on production
    const url = `http://geokgs.gharysh.kz/confirm?upn=${token}&locale=${this.locale}`

    // 3 - choose template
    let template;
    switch (this.locale) {
      case 'ru': template = 'verification_ru'; break;
      case 'kz': template = 'verification_kz'; break;
      default: template = 'verification_ru'; break;
    }
    // send verification mail
    const transporter = nodemailer.createTransport({
      host: process.env.MAIL_SMTP_ADDRESS,
      port: process.env.MAIL_SMTP_PORT,
      secure: true, 
      tls: { 
        rejectUnauthorized: false 
      },
      auth: {
        user: process.env.MAIL_LOGIN, 
        pass: process.env.MAIL_PASSWORD, 
      },
    });

    const email = new Email({
        message: {
          from: process.env.MAIL_FROM,
        },
        send: true,
        transport: transporter,
        preview: false,
      });
  
    await email
      .send({
        template,
        message: {
          to: this.email,
        },
        locals: {
          name: this.first_name,
          url: url,
        },
      });
    }

    static async decoded(userJwt) {
        return jwt.verify(userJwt, process.env.SECRET_KEY, (error, res) => {
          if (error) {
            throw error;
          }
          return new User(res);
        });
      }
}

class UsersController{
    /**
     * @api {post} /users Register a new user
     * @apiVersion 2.0.0
     * @apiGroup Users
     * @apiHeader Content-Type=application/x-www-form-urlencoded
     * @apiParam {String} email User email
     * @apiParam {String} first_name User first name, restriction: min:2 max:32
     * @apiParam {String} last_name User last name, restriction: min:2 max:32
     * @apiParam {String} [birth_date] User birthday, use ISO8601
     * @apiParam {String} [gender] User gender, accepted: 'male', 'female'
     * @apiParam {String} password User password: 
     * <br> *should contain at least one digit
     * <br> *should contain at least one lower case
     * <br> *should contain at least one upper case
     * <br> *should contain at least 8 from the mentioned characters
     * @apiParamExample {json} body
     *    {
     *      "email":"nurbolatsnk@gmail.com",
     *      "first_name": "Nurbolat",
     *      "last_name": "Sansyzbay",
     *      "birth_date": "1996-11-14",
     *      "gender": "male",
     *      "password": "Nurbolat123"
     *    }
     * @apiSuccess {Boolean} success User registration status
     * @apiSuccess {String} message Message description
     * @apiSuccessExample {json} Success
     *      HTTP/1.1 200 OK
     *      {
     *          "success": true,
     *          "message": "VerifyEmailAddress",
     *       }
     * @apiError {Boolean} success User registration status
     * @apiError {Object} error Error object
     * @apiError {String} error.message The error message, possible values:
     * <br> `InvalidEmail` 
     * <br> `InvalidPassword`
     * <br> `InvalidFirstName`
     * <br> `InvalidLastName`
     * <br> `InvalidGender`
     * <br> `InvalidLocale`
     * <br> `InternalServer` - Internal server error
     * <br> *`parameterName` - will return if someone pass non accepted parameters
     * @apiErrorExample {json} Error-Response:
     *     HTTP/1.1 400 Bad Request
     *     {   
     *      "success": false,
     *       "error": {
     *          message: "InvalidPassword"
     *         }
     *     }
     */

     static async register(req, res, next) {
         try {
            const UserSchema = Joi.object().keys({
                email: Joi.string().email().required().label('InvalidEmail'),
                password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/).required().label('InvalidPassword'),
                first_name: Joi.string().alphanum().min(1).max(32).required().label('InvalidFirstName'),
                last_name: Joi.string().alphanum().min(1).max(32).required().label('InvalidLastName'),
                birth_date: Joi.string().isoDate().allow(null).label('InvalidBirthDate'),
                gender: Joi.string().valid('male','female').label('InvalidGender'),
                locale: Joi.string().valid('ru','kz','en').label('InvalidLocale').default('ru')
            });

            const userFromBody = {...req.body};
            
            const validationResult = UserSchema.validate(userFromBody);
            if(validationResult.error){
                return res.status(400).json({
                    success: 'false',
                    errors:[{
                        message: validationResult.error.details[0].context.label
                    }]
                });
            }

            //get user
            const getUserResult = await UsersDAO.getUser({email: userFromBody.email});
            if(getUserResult.error){
              throw getUserResult.error;
            }
            if(getUserResult.user){
                return res.status(400).json({
                    success: false,
                    errors: [{
                        message: 'UserAlreadyExists'
                    }]
                });
            }
            //create user
            const user = new User({
              ...validationResult.value,
              password: await hashPassword(validationResult.value.password)
            });
            //create user in db
            let addUserResult = await UsersDAO.addUser(user);
            if(!addUserResult.success){
                 throw addUserResult.error;
            }

            user.sendVerificationMail();
            res.json({
                success: true,
                message: 'VerifyEmailAddress'
            });
         } catch (error) {
             next(error)
         }
     }


    /**
     *  @api {post} /wf?upn=<TOKEN> Verify email
     *  @apiVersion 2.0.0
     *  @apiGroup Users
     *  @apiParam {String} TOKEN Verification token
     *  @apiParamExample url
     *  baser_url/users/wf?upn=some_token
     *  @apiSuccess {Boolean} success Verification status
     *  @apiSuccess {String} message Verification message
     *  @apiSuccessExample {jsos} Success
     *  HTTP/1.1 200 OK
     *  {
     *    "success": true,
     *    "message": "EmailVerified"
     *  }
     *  @apiError {Boolean} success Verification status
     *  @apiError {Object} error Error object
     *  @apiError {String} error.message Error message, possible values:
     * <br> `InvalidToken` - Either fake token or it was expired
     * <br> `UserNotFound` - User was deleted because user does not verify email in 12 hours
     * <br> `UserAlredyVerified`
     * <br> `InternalServer`
     * @apiErrorExample {json} Error-Response:
     *  HTTP/1.1 400 Bad Request
     *     {  
     *       "success": false,
     *       "error": {
     *          "message": "InvalidToken"
     *         }
     *     }
     */
     static async verifyEmail(req, res) {
         try {
             const userJwt = req.body.upn;
             const validationResult = Joi.string().min(10).validate(userJwt);
             if(validationResult.error){
                 return res.status(400).json({
                     success: false,
                     errors: [{
                         message: "InvalidToken"
                     }]
                 });
             }

             let user;

             try {
                 const userFromJwt = await jwt.verify(userJwt, process.env.SECRET_KEY);
                 user = new User(userFromJwt);
             } catch (error) {
                return res.status(400).json({
                    success: false,
                    errors: [{
                        message: "InvalidToken"
                    }]
                });
             }

             //get user from db
             const getUserResult = await UsersDAO.getUser({email: user.email});
             if(getUserResult.error) {
               throw getUserResult.error;
             }

             if(!getUserResult.user){
                 return res.status(404).json({
                    success: false,
                    errors: [{
                        message: "UserNotFound"
                    }]
                 });
             }

             if(getUserResult.user.is_verified){
                 return res.status(400).json({
                     success: false,
                     errors: [{
                         message: "UserAlredyVerified"
                     }]
                 });
             }

             const verifyResult = await UsersDAO.verifyUser({email: getUserResult.user.email});
             if(!verifyResult.success){
                 throw verifyResult.error;
             }
             res.json({
               success: true,
               message: "EmailVerified"
             });

         } catch (error) {
           debug(`Error in ${ UsersController.verifyEmail.name}: ${ error.message}`)
           next(error);
         }
     }


     /**
      * @api {post} users/mail?email=<email> resendVerificationEmail
      * @apiGroup Users
      * @apiVersion 2.0.0
      * @apiParam {String} email User email for verification mail
      * @apiSuccess {Boolean} success Resend verification mail status
      * @apiSuccess {String} message Resen message
      * @apiSuccessExample {json} Success
      *   HTTP
      *    {
      *     "success": true,
      *     "message": "VerifyEmail"
      *   }
      * @apiError {Boolean} success Resend verification mail status
      * @apiError {Object} error Error object
      * @apiError {String} error.message The error message, possible values:
      * <br> `InvalidEmail`
      * <br> `UserNotFound`
      * <br> `UserAlreadyVerified`
      * <br> `InternalServer`
      * @apiErrorExample {json} Error-Response:
      *     {
      *       "success": false,
      *       "error": {
      *         "message": "InvalidEmail"
      *       }
      *     }
      */

     static async resendVerificationEmail(req, res, next) {
        try {
          const email = req.body.email;

          const validationResult = Joi.string().email().validate(email);

          if(validationResult.error){
            return res.status(400).json({
              success: false,
              errors: [{
                message: "InvalidEmail"
              }]
            });
          }

          const getUserResult = await UsersDAO.getUser({email});
          if(getUserResult.error) {
            throw getUserResult;
          }

          if(!getUserResult.user){
            return res.status(404).json({
              success: false,
              errors: [{
                message: "UserNotFound"
              }]
            });
          }

          if(getUserResult.user.is_verified){
            return res.status(400).json({
              success: false,
              errors: [{
                message: "UserAlreadyVerified"
              }]
            });
          }

          const user = new User(getUserResult.user);
          await user.sendVerificationMail();
          res.json({
            success: true,
            message: "VerifyEmail"
          });
        } catch (error) {
          debug(`Error in ${ UsersController.resendVerificationEmail.name}: ${ error.message}`)
          next(error);
        }
     }


    static async login(req, res, next) {
      try {
      const userFromBody = {...req.body}
      const LoginUserSchema = Joi.object().keys({
        permanent: Joi.boolean().label('InvalidPermanent'),
        email: Joi.string().email({minDomainSegments: 2}).required().label('InvalidEmail'),
        password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/).required().label('InvalidPassword'),
      });

      const validationResult = LoginUserSchema.validate(userFromBody);
      if (validationResult.error){
        return res.status(400).json({
          success: false,
          errors: [{
            message: validationResult.error.details[0].context.label
          }]
        });
      }
      
      const dbGetUserResponse = await UsersDAO.getUser({email: userFromBody.email});
      if(dbGetUserResponse.error){
        throw dbGetUserResponse.error;
      }

      if(!dbGetUserResponse.user){
        return res.status(404).json({
          success: false,
          errors: [{
            message: "UserNotFound"
          }]
        });
      }

      if(!dbGetUserResponse.user.is_verified){
        return res.status(400).json({
          success: false,
          errors: [{
            message: "UserNotVerified",
          }]
        });
      }

      const user = new User(dbGetUserResponse.user);

      const isValidPassword = await user.comparePassword(validationResult.value.password);

      if (!isValidPassword) {
        return res.status(401).json({
          success: false,
          errors: [
            {
              message: "InvalidPassword"
            }
          ]
        });
      }

      const authToken = user.encoded();

      var agent = useragent.parse(req.headers['user-agent']);
      let ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

      const dbSession = await UsersDAO
        .createSession({ 
          userId: user.id,
          agent: agent,
          ip: ipAddress
        });
      
      if( dbSession.error) {
        throw dbSession.error;
      }

      if (validationResult.value.permanent){
        const refreshToken = await cache.generateRefreshToken();
        cache.save(authToken, refreshToken, user);
        res.json({
          success: true,
          auth_token: authToken,
          refresh_token: refreshToken,
          user: user.toJson(),
        });
      } else {
        cache.save(authToken, null, user);
        res.json({
          success: true,
          auth_token: authToken,
          user: user.toJson(),
        });
      }
      

      } catch (error) {
      next(error);
      }
    }

     
   static async  apiPostRefreshToken(req, res, next) {
     try {
       const userDataFromBody = {...req.body};

       const UserDataSchema = Joi.object().keys({
         email: Joi.string().email().required().label('InvalidEmail'),
         refresh_token: Joi.string().required().label('InvalidRefreshToken')
       });

       const userDataValidation = UserDataSchema.validate(userDataFromBody);
       
       if( userDataValidation.error ){
        return res.status(400).json({
          success: false,
          errors: [{
            message: validationResult.error.details[0].context.label
          }]
        });
       }

       const userTokens = await cache.getTokens(userDataFromBody.email);
       
       if ( !userTokens || userTokens.refreshToken !== userDataFromBody.refresh_token){
         return res.status(400).json({
           success:false,
           errors: [{
             message: 'InvalidRefreshToken'
           }]
         });
       }

       const userFromDB = await UsersDAO.getUser({email: userDataFromBody.email});
       if ( userFromDB.error ) {
         throw userFromDB.error;
       }
       if ( !userFromDB.user ) {
         return res.status(404).json({
           success: false,
           errors: [{
             message: "UserNotFound"
           }]
         });
       }
       const user = new User(userFromDB.user);
       const authToken = await user.encoded();
       const refreshToken = await cache.generateRefreshToken();
       cache.save(authToken, refreshToken, user);
       res.json({
         success: true,
         auth_token: authToken,
         refresh_token: refreshToken,
         user: user.toJson()
       })
     } catch (error) {
      debug(`Error in ${ UsersController.apiPostRefreshToken.name}: ${ error.message}`)
      next(error);
     }
   }


   static async apiGetUser(req, res, next) {
     try {
       const userId = req.params.userId;
       const userIdValidation = Joi
        .number()
        .integer()
        .required()
        .validate(userId);

      if( userIdValidation.error ){
        return res.status(400).json({
          succcess: false, 
          errros: [{
            message: 'InvalidUserId'
          }]
        });
      }

      const dbUser = await UsersDAO.getUser({id: userId});

      if ( dbUser.error ){
        throw dbUser.error;
      }
      if ( !dbUser.user ){
        return res.status(404).json({
          success: false,
          errors: [{
            message: 'NotFound'
          }]
        });
      }

      const user = new User(dbUser.user);
      res.status(200).json({
        success: true,
        user: user.toJson()
      });
     } catch (error) {
       next(error);
     }
   }


   static async apiGetUsersBySearch(req, res, next) {
     try {
      const searchFromQuery = { ...req.query };
      const searchValidation = Joi
          .object()
          .keys({
              query: Joi
                  .string()
                  .min(1)
                  .required(),
              page: Joi
                  .number()
                  .integer()
                  .min(1),
              size: Joi
                  .number()
                  .integer()
                  .min(1)
          })
          .required()
          .validate(searchFromQuery);

        if ( searchValidation.error ){
            const errors = getListOfObjectsWithErrorMessages(searchValidation.error, ['page', 'size', 'page'] );
            return res.status(400).json({
                success: true,
                errors
            });
        }

        const {query, page, size} = searchValidation.value

        const dbUsers = await UsersDAO.getUserBySearch(query, page, size);
        
        if(dbUsers.error) {
          throw dbUsers.error;
        }

        res.json({
          success: true,
          users: dbUsers.value
        });
     } catch (error) {
       next(error);
     }
   }


   static async  apiGetGeo(req, res, next) {
    try {
      let ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
      //FIXME: replace fixed ip with var 'ipAddress'
      var geo = geoip.lookup('85.117.127.249');
      res.json({
        success: true,
        geo: geo
      })
    } catch (error) {
      next(error)
    }
   }


   static  get _uploadProfileImage() {
      const acceptedFileExtensions = ['png', 'jpg', 'jpeg', 'webp'];
      return multer({
      storage: profileImages.storage,
      limits: {
        fileSize: 2 * 1024 * 1024,
        files: 1,
      },
      fileFilter(req, file, cb) {
        const re = /(?:\.([^.]+))?$/;
        let ext = re.exec(file.originalname)[1] || '';
        ext = ext.toLowerCase();
    
        if (acceptedFileExtensions.indexOf(ext) === -1) {
          cb(null, false);
          cb(new Error('File format error'));
        } else {
          cb(null, true);
        }
      },
    
    }).single('image');
  }


  static async apiUpdateProfileImage(req, res, next) {
    UsersController._uploadProfileImage(req, res, async (err) => {
      try {
        if ( err ){
            let errorMessage;
            switch ( err.message ){
                case 'File too large': errorMessage = 'InvalidFileSize';
                    break;
                case 'FileFormatError': errorMessage = 'InvalidFileFormat';
                    break;
                default: errorMessage = 'InvalidFile'
            }
            return res.status(400).json({
                success: false,
                errors: [
                    {
                        message: errorMessage
                    }
                ]
            });
        }
        

        const userId = req.params.userId;
        const user = req.user;

        if ( user.role === Roles.User && user.id != userId){
          await profileImages.deleteImage(req.file.filename);
          return res.status(403).json({
            success: false,
            errors: [{
              message: "PermissionDenied"
            }]
          });
        }

        const dbUser = await UsersDAO.getUser({id: userId});

        if ( dbUser.error ){
          throw dbUser.error;
        }
        
        if ( !dbUser.user ) {
          await profileImages.deleteImage(req.file.filename);
          return res.status(404).json({
              success: false,
              errors: [{
                message: "UserNotFound"
              }]
          });
        }

        const userObject = new User(dbUser.user);
        if (userObject.image){
            try {
                await profileImages.deleteImage(user.image);
            } catch (error) {
                if (error.code !== 'ENOENT'){
                    throw error;
                }
            }
        }

        const newUserWithOnlyImageField = {
        image: req.file.filename,
        image_url: '/uploads' + profileImages.dirName + '/' + req.file.filename
        }
        
        const dbUserUpdated = await UsersDAO.updateUser(userId, newUserWithOnlyImageField);
        if (dbUserUpdated.error){
            throw dbUserUpdated.error;
        }

        const newUser = new User(dbUserUpdated.value);

        res.json({
            success: true,
            user: newUser.toJson()
        });
      } catch (error) {
           next( error );
      }
    });
  }


  static async apiGetUserAll(req,res,next) {
    try {
      const paginationFromQuery = req.query;
      const paginationSchema = Joi.object().keys({
        page: Joi.number().min(1).required(),
        size: Joi.number().min(1).max(100).required()
      }); 
      const paginationValidation = paginationSchema.validate(paginationFromQuery, {abortEarly: false});
      if (paginationValidation.error) {
        const errors = getListOfObjectsWithErrorMessages(
          paginationValidation.error,
          ['page','size']
        );
        return res.status(400).json({
          success: false,
          errors
        });
      }

      const page = paginationValidation.value.page;
      const size = paginationValidation.value.size;


      const dbUsers = await UsersDAO.getUserAll(page, size);

      if ( dbUsers.error ){
        throw dbUsers.error;
      }

      res.json({
        success: true,
        users: dbUsers.value.results,
        total: dbUsers.value.total
      });
    } catch (error) {
      next(error);
    }
  }


  static async apiPostUser(req, res, next) {
    try {
      const userSchema = Joi.object().keys({
          email: Joi.string().email().required().label('InvalidEmail'),
          password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/).required().label('InvalidPassword'),
          first_name: Joi.string().alphanum().min(1).max(32).required().label('InvalidFirstName'),
          last_name: Joi.string().alphanum().min(1).max(32).required().label('InvalidLastName'),
          birth_date: Joi.string().isoDate().allow(null).label('InvalidBirthDate'),
          gender: Joi.string().valid('male','female').label('InvalidGender'),
          locale: Joi.string().valid('ru','kz','en').label('InvalidLocale').default('ru')
      });

      const userFromBody = {...req.body};
      
      const validationResult = userSchema.validate(userFromBody);
      if(validationResult.error){
          return res.status(400).json({
              success: 'false',
              errors:[{
                  message: validationResult.error.details[0].context.label
              }]
          });
      }

      //get user
      const getUserResult = await UsersDAO.getUser({email: userFromBody.email});
      if(getUserResult.error){
        throw getUserResult.error;
      }
      if(getUserResult.user){
          return res.status(400).json({
              success: false,
              errors: [{
                  message: 'UserAlreadyExists'
              }]
          });
      }

      //create user
      const user = new User({
        ...validationResult.value,
        is_verified: true,
        password: await hashPassword(validationResult.value.password)
      });

      
      //create user in db
      let addUserResult = await UsersDAO.addUser(user);
      if(!addUserResult.success){
           throw addUserResult.error;
      }

      res.json({
          success: true,
          user: addUserResult.value
      });
   } catch (error) {
       next(error)
   }
  }


  static async apiPatchUser(req, res, next) {
    try {
      const user = req.user;
      const updatedUserFromBody = { ...req.body }
      const userId = req.params.userId

      if ( user.id != userId && user.role !== Roles.Admin ){
        return res.status(403).json({
          success: false,
          errors: [{
            message: 'PermissionDenied'
          }]
        });
      }
      
      const userSchema = Joi.object().keys({
        password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/),
        first_name: Joi.string().alphanum().min(1).max(32),
        last_name: Joi.string().alphanum().min(1).max(32),
        birth_date: Joi.string().isoDate(),
        gender: Joi.string().valid('male','female'),
        locale: Joi.string().valid('ru','kz','en'),
        role: Joi.string().valid(...Object.values(Roles))

      });



      const updatedUserValidation = userSchema.validate(updatedUserFromBody, { abortEarly: false });
      
      if ( updatedUserValidation.error ) {
        const errors = getListOfObjectsWithErrorMessages(
          updatedUserValidation.error,
          ['password','first_name','last_name','birth_date','gender','locale']
        )
        return res.status(400).json({
          success: false,
          errors: errors
        });
      }

      let updatedUser = updatedUserValidation.value;

      if (updatedUser.role && user.role === Roles.User) {
        delete updatedUser.role
      }

      const dbUpdatedUser = await UsersDAO.updateUser(userId, updatedUser);

      if ( dbUpdatedUser.error ){
        throw dbUpdatedUser.error;
      }

      res.json({
        success: true,
        user: dbUpdatedUser.value
      });
      
    } catch (error) {
      next( error )
    }
  }


  static async apiLogoutUser(req, res, next) {
    try {
      const user = req.user;
      await cache.deleteToken(user.email);
      res.json({
        success: true
      });
    } catch (error) {
      next ( error )
    }
  }


  static async apiDeleteUser(req, res, next) {
    try {
      const userIdFromParams = req.params.userId;

      const userIdValidation = Joi
        .number()
        .integer()
        .min(0)
        .required()
        .validate(userIdFromParams);

      if ( userIdValidation.error ){
        return res.status(400).json({
          success: false,
          errors: [{
            message: "InvalidUserId"
          }]
        });
      }

      const dbDeleteUser = await UsersDAO.deleteUserAdmin(userIdFromParams);

      if ( dbDeleteUser.error ) {
        throw dbDeleteUser.error;
      }

      res.json({
        success: true
      });
    } catch (error) {
      next ( error )
    }
  }
}




module.exports = {
    User,
    UsersController
};