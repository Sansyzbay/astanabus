const express = require('express');

const router = express.Router();

const UsersCtrl = require('./users.controller').UsersController;
const oauth = require('../_helpers/authentication');
const permission = require('../_helpers/permission');
const Roles = require('../_helpers/roles');

    
router.route('/users/')
    // TODO: check post user
    .post(oauth,permission(Roles.Admin),UsersCtrl.apiPostUser)
    .get(oauth, UsersCtrl.apiGetUserAll);
router.route('/users/register').post(UsersCtrl.register)
router.route('/users/wf').patch(UsersCtrl.verifyEmail);
router.route('/users/login').post(UsersCtrl.login)
router.route('/users/logout').post(UsersCtrl.apiLogoutUser);
router.route('/users/mail').post(UsersCtrl.resendVerificationEmail);
router.route('/users/refresh-token').post(UsersCtrl.apiPostRefreshToken);
router.route('/users/search').get(oauth, UsersCtrl.apiGetUsersBySearch);
router.route('/users/geo').get(UsersCtrl.apiGetGeo);
router.route('/users/:userId')
    .get(oauth, UsersCtrl.apiGetUser)
    //TODO: check patch user
    .patch(oauth, UsersCtrl.apiPatchUser)
    .delete(oauth, permission(Roles.Admin), UsersCtrl.apiDeleteUser);
router.route('/users/:userId/image').patch(oauth, UsersCtrl.apiUpdateProfileImage);
module.exports = router;
