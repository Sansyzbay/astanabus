
const User = require('../models/User');


module.exports = class UsersDAO {


    static async addUser(user){
      try {
        const result = await User
        .query()
        .insert(user);
        return { 
          success: true,
          value: result
         }
      } catch (error) {
        return { error }
      }
    }


    static async getUser({email, id}){
      try {
        let user;
        if (email) {
          user = (await User
            .query()
            .where('email',email))[0];
        }else {
          user = await User
            .query()
            .findById(id);
        }
        return { user: user }
      } catch (error) {
        return { error }
      }
    }


    static async verifyUser({email}){
      try {
        await User
          .query()
          .patch({is_verified: true})
          .where('email', email);
          return {
            success: true
          }
      } catch (error) {
        return { error }
      }
    }


    static async createSession({userId, ip, agent, isPermanent}){
      try {
         let user = await User
          .query()
          .findById(userId);
         let session = await user
          .$relatedQuery('sessions')
          .insert({
            user_id: userId,
            agent: agent,
            ip_address: ip,
          });
          return {
            success: true,
            value: session
          }
      } catch (error) {
        return { error }
      }
    }

     
    static async getUserBySearch(search, page = 1, size = 100){
      try {
          const users = await User
            .query()
            .select('id', 'first_name', 'last_name', 'username', 'email', 'is_verified', 'gender', 'locale', 'birth_date', 'image', 'image_url', 'role', 'region', 'created_at', 'updated_at')
            .whereRaw("email ILIKE ?",[`%${search}%`])
            .orWhereRaw("first_name ILIKE ?",[`%${search}%`])
            .orWhereRaw("last_name ILIKE ?",[`%${search}%`])
            .orWhereRaw("username ILIKE ?",[`%${search}%`])
            .page((page - 1),size);
          return {
            success: true,
            value: users
          }
      } catch (error) {
          return { error }
      }
    }
    
    
    static async updateUser(userId, newUser){
      try {
        const user = await User
          .query()
          .patchAndFetchById(userId,newUser);
        
        return {
          success: true,
          value: user
        }
      } catch (error) {
        return { error }
      }
    }


    static async getUserAll(page, size){ 
      try {
          const result = await User
              .query()
              .select('id', 
                'first_name', 
                'last_name', 
                'username', 
                'email', 
                'is_verified', 
                'gender', 
                'locale', 
                'birth_date', 
                'image', 
                'image_url', 
                'role', 
                'region', 
                'created_at', 
                'updated_at')
              .orderBy('first_name','ASC')
              .orderBy('last_name','ASC')
              .page((page - 1), size);
          return {
              success: true,
              value: result
          }
      } catch (error) {
          return { error }
      }
  }


  static async deleteUserAdmin(userId){
    try {
      await User
        .query()
        .deleteById(userId);
      return { success: true }
    } catch (error) {
      return { error }
    }
  }
}