/**
 * 
 * @param {Object} validationError 
 * @param {Array} validFieldNames 
 * @return {Array}
 */
function getListOfObjectsWithErrorMessages(validationError,validFieldNames){
    let errors = [];
    const { details } = validationError;
    for ( let index = 0; index < details.length; index++ ) {
        const fieldError = details[index];
        const ROOT_FIELD = 0;
        const fieldName = details[index].path[ROOT_FIELD];
        
        if( validFieldNames.indexOf(fieldName) !== -1 ){
            const subNames = fieldName.split('_');
            const capSubNames = subNames.map(subName => toCapital(subName));
            const capFieldName = capSubNames.join("");
            const message = 'Invalid' + capFieldName;
            errors.push({ message });
        } else {
            const message = 'UnknowKey'
            const key = fieldError.path[ROOT_FIELD];
            errors.push({ message, key });
        }
    }
    return errors;
}


function toCapital(text){
    const toCap = text.charAt(0).toUpperCase() + text.slice(1);
    return toCap;
}

module.exports = getListOfObjectsWithErrorMessages;