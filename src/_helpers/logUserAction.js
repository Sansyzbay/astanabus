const UserActionsDAO = require('../dao/userActionsDAO');


async function logUserAction(req, res, next){
    try {

        const user = req.user;
        const method = req.method;
        const request_body = { ...req.body };
        const request_query = { ...req.query };
        const request_params = { ...req.params };
        const request_headers = { ...req.headers };
        const route = req.originalUrl;
        var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;


        if (request_headers['authorization']){
            request_headers['authorization'] = "***Hidden***";
        }else if ( request_headers['Authorization']) {
            request_headers['Authorization'] = "***Hidden***";
        }

        if (request_body && request_body['password']) { 
            request_body['password'] = "***Hidden***";
        }

        const userAction = {
            user_id: user ? user.id : null,
            method: method,
            request_body: request_body,
            request_headers: request_headers,
            request_params: request_params,
            request_query: request_query,
            route: route,
            ip: ip
        }


        UserActionsDAO.addUserAction(userAction);

        next();
    } catch (error) {
        next();
    }
}

module.exports = logUserAction;