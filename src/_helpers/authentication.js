const Joi = require('joi');
const { User } = require('../api/users.controller');
const cache = require('../../redis');

module.exports = async function(req, res, next){
    if (!req.get('Authorization') || !req.get('authorization') ) {
        return res.status(403).json({
          success: false,
          errors: [
            {
            message: 'InvalidToken'
          } 
        ]
        });
      }
      let userJwt 
      if(req.get('Authorization')){
        userJwt = req.get('Authorization').slice('Bearer '.length);
      }else{
        userJwt = req.get('authorization').slice('Bearer '.length);
      }
      let userObj
      try {
        userObj = await User.decoded(userJwt);
        const tokens = await cache.getTokens(userObj.email);
        if ( !tokens || tokens.token !== userJwt) {
          throw new Error('Token does not exists');
        }
      } catch (error) {
        return res.status(401).json({ 
          success: false, 
          errors: [{ message: "InvalidToken"}]
        });
      }
      if(!req.user) req.user = userObj;
      next();
}