'use strict'

const { Model } = require('objection');

class User extends Model {
    static get tableName(){
        return 'users';
    }

    $beforeUpdate() {
        this.updated_at = new Date().toISOString();
    }

    static get modifiers() {
        return {
          withoutPassword(builder) {
            builder.select('users.id', 
                'users.first_name', 
                'users.last_name', 
                'users.username', 
                'users.email', 
                'users.is_verified', 
                'users.gender', 
                'users.locale', 
                'users.birth_date', 
                'users.image', 
                'users.image_url', 
                'users.role', 
                'users.region', 
                'users.created_at', 
                'users.updated_at');
          }
        };
      }

    static get relationMappings() {
        return{

        }
    }
}

module.exports = User
