const path  = require('path');
require('dotenv').config({path: path.resolve('.myenv')})

module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: process.env.DEV_DB_DATABASE,
      user: process.env.DEV_DB_USER,
      password: process.env.DEV_DB_PASSWORD,
      host: process.env.DEV_DB_HOST,
      port: process.env.DEV_DB_PORT
    }
  },

  //IMPORTANT: There is no staging 
  staging: {
    client: 'postgresql',
    connection: {
      database: 'geoportal_staging',
      user:     'postgres',
      password: 'mysecretpassword',
      host: 'localhost',
      port: '2345'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: process.env.DB_DATABASE,
      user: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
